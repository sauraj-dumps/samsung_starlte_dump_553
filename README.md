## starltexx-user 10 QP1A.190711.020 G960FXXUGFUH3 release-keys
- Manufacturer: samsung
- Platform: universal9810
- Codename: starlte
- Brand: samsung
- Flavor: starltexx-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: G960FXXUGFUH3
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: samsung/starltexx/starlte:10/QP1A.190711.020/G960FXXUGFUH3:user/release-keys
- OTA version: 
- Branch: starltexx-user-10-QP1A.190711.020-G960FXXUGFUH3-release-keys
- Repo: samsung_starlte_dump_553


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
